function initTabNav() {
    const tabMenu = document.querySelectorAll('.js-tabmenu li');
    const tabContent = document.querySelectorAll('.js-tabcontent section');

    // se usar em outra página e não tiver nada nesses elementos, vai dar erro
    if (tabMenu.length && tabContent) {
        function active(index) {
            tabContent.forEach((section) => {
                section.classList.remove('active');
            });
            tabContent[index].classList.add('active');
        }

        tabMenu.forEach((itemMenu, index) => {
            itemMenu.addEventListener('click', function () {
                active(index);
            });
        });
    }
}

initTabNav();

function initAccordian() {
    const accordionList = document.querySelectorAll('.js-accordion dt');

    if (accordionList.length) { // se existe, executo
        accordionList.forEach((item) => {
            item.addEventListener('click', activeAccordion);
        });

        accordionList[0].classList.add('active'); // o primeiro já aparecer
        accordionList[0].nextElementSibling.classList.add('active');

        function activeAccordion() {
            // accordionList.forEach((section) => {       esconde quando clico em outro item
            //     section.nextElementSibling.classList.remove('active');
            // });
            this.classList.toggle('active'); // active no dt
            this.nextElementSibling.classList.toggle('active'); // this faz referência ao item, ativo no próximo elemento, q é dd
        }
    }
}
initAccordian();
